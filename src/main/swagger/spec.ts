/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */

/**
 * Create the definition of an API response schema
 *
 * @param payload Link to the definition of response's data schema
 */
function createApiResponseDefinition(payload: string) {
    return {
        "required": [ "data", "error" ],
        "properties": {
            "data": {
                "$ref": payload
            },
            "error": {
                "type": "string",
                "nullable": true
            }
        }
    };
}

export { createApiResponseDefinition };
