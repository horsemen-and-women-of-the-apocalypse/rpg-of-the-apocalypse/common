/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */

/**
 * Abstract class for API responses
 */
class AResponse<T> {
    /** Data related to the response */
    public readonly data: T;

    /** String describing the error if it exists */
    public readonly error: string | null;

    /**
     * Constructor
     *
     * @param data Data
     * @param error Error (optional)
     */
    constructor(data: T, error: string | null = null) {
        this.data = data;
        this.error = error;
    }
}

/**
 * Basic API response
 */
class ApiResponse<T> extends AResponse<T> {
    /**
     * Constructor
     *
     * @param data Data
     */
    constructor(data: T) {
        super(data);
    }
}

/**
 * API response representing an error
 */
class ApiErrorResponse extends AResponse<string> {
    /**
     * Constructor
     *
     * @param error Error
     */
    constructor(error: string) {
        super("KO", error);
    }
}

export { ApiResponse, ApiErrorResponse };
