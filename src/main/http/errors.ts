/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */

/**
 * Error with an HTTP status code
 */
class ApiError extends Error {
    /** HTTP status code */
    public readonly code: number;

    /**
     * Constructor
     *
     * @param message Message
     * @param code HTTP status code
     */
    constructor(message: string, code: number) {
        super(message);
        this.code = code;
    }
}

export { ApiError };
