/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { describe, it } from "mocha";
import { createApiResponseDefinition } from "../../main";
import { expect } from "chai";

describe("Swagger", () => {
    it("#createApiResponseDefinition", () => {
        const type = "MyType";
        const def = createApiResponseDefinition(type);

        expect(def).to.be.not.null.and.to.be.not.undefined;
        expect(def.properties).to.be.not.null.and.to.be.not.undefined;
        expect(def.properties.data).to.be.not.null.and.to.be.not.undefined;
        expect(def.properties.data.$ref).deep.eq(type, `Data is an instance of ${type}`);
    });
});