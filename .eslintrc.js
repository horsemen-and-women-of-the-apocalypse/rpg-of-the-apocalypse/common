const isProduction = process.env.NODE_ENV === "production";

module.exports = {
    root: true,
    parser: "@typescript-eslint/parser",
    "plugins": [
        "@typescript-eslint"
    ],
    extends: [
        "eslint:recommended",
        "plugin:@typescript-eslint/recommended"
    ],
    rules: {
        "no-console": isProduction ? "error" : "off",
        "no-debugger": isProduction ? "error" : "off",
        "array-bracket-spacing": [ "error", "always" ],
        "object-curly-spacing": [ "error", "always" ],
        "linebreak-style": [ "error", "unix" ],
        "space-before-function-paren": [ "error", "never" ],
        indent: [ "error", 4, { SwitchCase: 1 } ],
        quotes: [ "error", "double" ],
        semi: [ "error", "always" ]
    }
};
